package com.zenika.academy.videogames.controllers;

import com.zenika.academy.videogames.controllers.representation.VideoGameNameRepresentation;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.service.VideoGamesService;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/video-games")
public class VideoGamesController {

    private VideoGamesService videoGamesService;
    private VideoGame videoGame;

    @Autowired
    public VideoGamesController(VideoGamesService videoGamesService) {
        this.videoGamesService = videoGamesService;
    }

    /**
     * Récupérer la liste des jeux vidéos possédés pas l'utilisateur
     * <p>
     * Exemple :
     * <p>
     * GET /video-games
     */
    @GetMapping
    public List<VideoGame> listOwnedVideoGames() {
        return videoGamesService.ownedVideoGames();
    }

    /**
     * Récupérer un jeu vidéo par son ID
     * <p>
     * Exemple :
     * <p>
     * GET /video-games/3561
     */
    @GetMapping("/{id}")
    /** public ResponseEntity<VideoGame> getOneVideoGame(@PathVariable("id") Long id) {
     VideoGame foundVideoGame = this.videoGamesService.getOneVideoGame(id);
     if(foundVideoGame != null) {
     return ResponseEntity.ok(foundVideoGame);
     }
     else {
     return ResponseEntity.notFound().build();
     }
     }*/

    /**
     * Ajouter un jeu vidéo à la collection de l'utilisateur
     *
     * Exemple :
     *
     * POST /video-games
     * Content-Type: application/json
     *
     * {
     *     "name": "The Binding of Isaac: Rebirth"
     * }
     */
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public VideoGame addVideoGameByName(@RequestBody VideoGameNameRepresentation videoGameName) {
        return this.videoGamesService.addVideoGame(videoGameName.getName());
    }

    @GetMapping
    public List listByGender(@RequestParam String videoGame) {
        return videoGamesService.ownedVideoGames();
    }

  /**  @DeleteMapping
    public Map<String, Boolean> deleteGame(@PathVariable Long id)
            throws ResourceNotFoundException {
        Optional<VideoGame> videoGame = videoGamesService.getOneVideoGame(id);

        if (videoGame != null) {
            videoGamesService.delete(id);//methode non faite faute de temps
        }
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
*/

    }
