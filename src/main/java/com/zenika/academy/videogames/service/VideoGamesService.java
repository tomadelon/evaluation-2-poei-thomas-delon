package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesService {

    private VideoGamesRepository videoGamesRepository;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository) {
        this.videoGamesRepository = videoGamesRepository;
    }

    public List<VideoGame> ownedVideoGames() {
        return this.videoGamesRepository.getAll();
    }

    public VideoGame getOneVideoGame(Long id) {
        return this.videoGamesRepository.get(id).orElseThrow();
    }

    public VideoGame addVideoGame(String name) {

        RawgDatabaseClient client = new RawgDatabaseClient();
        VideoGame newGame = client.getVideoGameFromName(name);

        videoGamesRepository.save(newGame);

        return newGame;
    }
}
