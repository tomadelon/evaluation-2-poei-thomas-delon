package com.zenika.academy.videogames.configuration;

import com.fasterxml.jackson.annotation.JsonCreator;

public class GameDTO {

    private String nameGame;

    @JsonCreator
    public GameDTO(String nameGame){
        this.nameGame = nameGame;
    }

    public String getNameGame() {
        return nameGame;
    }

    public void setNameGame(String nameGame) {
        this.nameGame = nameGame;
    }
}
